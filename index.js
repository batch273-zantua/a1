let arrnum = [15, 20,23,30,37];

arrnum.forEach(checkValidity);

function checkValidity(num){
	if(num % 5 == 0){
		console.log(num + " is divisible by 5");
	} else {
		console.log(false);
	}
}
console.log(Math);

/*

Activity - Quiz

1. let arr = [];
2. Array[0][0]
3. split the string to put in a new array and get the length and get the last char by addressing the last index of the new array
*/
	
	let array = ["John", "Joe", "Jane", "Jessie"];
	let lastEl = array[array.length-1];
	let newArr = lastEl.split('');
	let lastChar = newArr[newArr.length - 1]
	console.log(lastChar);
//4. indexOf method
//5. forEach method
//6. concat method
//7. Filter method
//8. some method
//9. false
//10. true


//Activity 2
//addToEnd(array, "Ryan");
function addToEnd(arr, str) {
  if (typeof str !== "string") {
    console.log("error - can only add strings to an array");
  } else {
    arr.push(str);
    console.log(arr);
  }
}

//Activity3
elementChecker(array, "Joe");

function elementChecker(arr, str){
	if(arr.length == 0){
		console.log("error - passed in array is empty")
	} else {
		if(arr.includes(str)){
			console.log(true);
		} else {
			console.log(false);
		}
	}
}

//Activity 4

function checkAllStringsEnding(arr, char){
	if(arr.length == 0){
		console.log("error - array must not be empty")
		return;
	} 

	for (let i = 0; i < arr.length; i++) {
    	if (typeof arr[i] !== "string") {
      		console.log( "error - all array elements must be strings");
      		return;
    	}
	}

	if(typeof char != "string"){
		console.log("error - 2nd argument must be of data type string");
		return;
	}

	if(char.length > 1){
		console.log("error - 2nd argument must be a single character");
		return;
	}

	let bool1 = true;
	for (let i = 0; i < arr.length; i++) {
    	if (!arr[i].endsWith(char)) {
      		bool1 = false;
    	} 
  	}
  	console.log(bool1);
  		
}

checkAllStringsEnding(array, "e");

//Activity 5


function stringLengthSorter(arr){
	let stringChecker = true;
	for (let i = 0; i < arr.length; i++) {
    	if (typeof arr[i] !== "string") {
      		stringChecker = false;
    	}
	}

	if(stringChecker = false){
		console.log( "error - all array elements must be strings");
	} else {

		console.log(arr.sort());
	}



}

stringLengthSorter(array);

// Activity 6

function startsWithCounter(arr, char){

	if(arr.length == 0){
		console.log("error - array must not be empty")
		return;
	} 
	for (let i = 0; i < arr.length; i++) {
    	if (typeof arr[i] !== "string") {
      		console.log( "error - all array elements must be strings");
      		return;
    	}
	}
	if(typeof char != "string"){
		console.log("error - 2nd argument must be of data type string");
		return;
	}

	 if(char.length > 1){
	 	console.log("error - 2nd argument must be a single character");
	 	return;
	 }

	let count = 0;

	for (let i = 0; i < arr.length; i++) {
    	if (arr[i].startsWith(char)) {
      		count += 1;
  		}
  	}
  	console.log(count);


}

startsWithCounter(array, "J");

//Activity 7

function likeFinder(arr, str){
	if(arr.length == 0){
		console.log("error - array must not be empty")
		return;
	} 
	for (let i = 0; i < arr.length; i++) {
    	if (typeof arr[i] !== "string") {
      		console.log( "error - all array elements must be strings");
      		return;
    	}
	}
	if(typeof str != "string"){
		console.log("error - 2nd argument must be of data type string");
		return;
	}


	let arr2 = [];

	for (let i = 0; i < arr.length; i++) {
    	if (arr[i].includes(str)) {
      		arr2.push(arr[i]);
  		}
  	}
  	console.log(arr2);

}

likeFinder(array, "jo");

function randomPicker(arr) {
  const randP = Math.floor(Math.random() * arr.length);
  console.log(arr[randP]);
}
randomPicker(array);






